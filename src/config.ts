import dotenv from 'dotenv';
dotenv.config();

export const PORT = process.env.PORT ?? 3001;
export const MONITORING_MONGO_HOST = process.env.MONITORING_MONGO_HOST ?? '';
export const MONITORING_MONGO_PORT = process.env.MONITORING_MONGO_PORT ?? '';
export const MONITORING_MONGO_USER = process.env.MONITORING_MONGO_USER ?? '';
export const MONITORING_MONGO_PASS = process.env.MONITORING_MONGO_PASS ?? '';
export const MONITORING_MONGO_DATABASE =
  process.env.MONITORING_MONGO_DATABASE ?? '';

export const RABBITMQ_PROTOCOL = process.env.RABBITMQ_PROTOCOL ?? '';
export const RABBITMQ_HOST = process.env.RABBITMQ_HOST ?? '';
export const RABBITMQ_PORT = process.env.RABBITMQ_PORT ?? '';
export const RABBITMQ_USER = process.env.RABBITMQ_USER ?? '';
export const RABBITMQ_PASS = process.env.RABBITMQ_PASS ?? '';
export const RABBITMQ_VHOST = process.env.RABBITMQ_VHOST ?? '';

export const HOST_AMQP = process.env.HOST_AMQP ?? '';
