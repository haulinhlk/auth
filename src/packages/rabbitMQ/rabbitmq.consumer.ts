/* eslint-disable @typescript-eslint/no-explicit-any */
import {bind, BindingScope, config} from '@loopback/core';
// import amqp, {Channel, Connection} from 'amqplib';
import amqp from 'amqplib/callback_api';
import {Replies} from 'amqplib/properties';
import {EventEmitter} from 'events';
import {
  Channel,
  ConfigDefaults,
  Connection,
  Options,
  RabbitmqBindings,
  RabbitmqComponentConfig,
} from './index';

type EventSubcriber = {
  [key: string]: (data: unknown) => void;
};

interface CIChannel extends Omit<Channel, 'assertQueue'> {
  assertQueue(
    queue: string,
    options?: Options.AssertQueue,
    callback?: (
      err: string,
      queueResponse: Replies.AssertQueue,
    ) => Promise<void>,
  ): Promise<Replies.AssertQueue>;
}
@bind({scope: BindingScope.SINGLETON})
export class RabbitmqConsumer extends EventEmitter {
  private connection: Connection | undefined;
  private channel: Channel | undefined;
  private timeoutId: NodeJS.Timeout;
  private retry: number;
  private readonly retries: number;
  private interval: number;

  constructor(
    @config({fromBinding: RabbitmqBindings.COMPONENT})
    private componentConfig: Required<RabbitmqComponentConfig>,
  ) {
    super();

    this.componentConfig = {...ConfigDefaults, ...this.componentConfig};

    const {retries, interval} = this.componentConfig.consumer;
    this.retry = 0;
    this.retries = retries;
    this.interval = interval;
  }

  async subscribeConsume(
    key: string,
    chanel: CIChannel,
    callback: (data: unknown) => void,
  ) {
    await chanel.assertExchange(key, 'fanout', {durable: false});
    await chanel.assertQueue('', {exclusive: true}, async (specialError, q) => {
      if (specialError) {
        console.log(specialError);
        return;
      }

      const queue = q.queue;

      console.log(
        ' [*] Waiting for messages in %s. To exit press CTRL+C',
        queue,
      );
      await chanel.bindQueue(queue, key, '');
      await chanel.consume(
        queue,
        msg => {
          if (msg?.content) {
            try {
              let message = msg.content !== null ? msg.content : '';
              message = message.toString();
              const parsedMessage = JSON.parse(message);
              console.log(parsedMessage, '==parsedMessage==');
              callback(parsedMessage);
              // callback(parsedMessage);
            } catch (error) {
              callback(undefined);
            }
          }
        },
        {noAck: true},
      );
    });
  }

  private connectAmqp(hostRabbitMQ: string, callback: Function) {
    amqp.connect(hostRabbitMQ, (error, conn) => {
      function close() {
        setTimeout(() => {
          conn.close();
        }, 500);
      }
      if (error) {
        console.log(error);
      } else {
        conn.createChannel((errorChanel, chanel) => {
          if (errorChanel) {
            console.log(errorChanel);
          } else {
            callback(conn, chanel, close);
          }
        });
      }
    });
  }
  private connectAmqpSubscribe(hostRabbitMQ: string, callback: Function) {
    this.connectAmqp(hostRabbitMQ, (conn: string, chanel: Channel) => {
      console.log('connectAmqp success');
      callback(conn, chanel);
    });
  }

  createAmqpSubcriber(hostRabbitMQ: string, subcribers: EventSubcriber) {
    this.connectAmqpSubscribe(
      hostRabbitMQ,
      (conn: unknown, chanel: CIChannel) => {
        Object.keys(subcribers).map(async key => {
          await this.subscribeConsume(key, chanel, subcribers[key]);
        });
      },
    );
  }
}
