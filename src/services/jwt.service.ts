// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {HttpErrors} from '@loopback/rest';
import jsonWebToken from 'jsonwebtoken';
import {User} from '../models';
export const JWT_SECRET = 'jsonparetoken';

type DecodeTokenType = {
  id: string;
  password: string;
};

export class JWTService {
  generateToken(userProfile: Pick<User, 'id' | 'password'>): string {
    if (!userProfile) {
      throw new HttpErrors.Unauthorized(
        'Error generating token : userProfile is null',
      );
    }
    // Generate a JSON Web Token
    let token: string;
    try {
      token = jsonWebToken.sign(userProfile, JWT_SECRET);
    } catch (error) {
      throw new HttpErrors.Unauthorized(`Error encoding token : ${error}`);
    }

    return token;
  }

  // decodeToken(token: string | undefined): string | null {
  //   if (!token) {
  //     throw new HttpErrors.Unauthorized('');
  //   }
  //   console.log('token jwt', token);
  //   try {
  //     const dataDecoded = jsonWebToken.verify(token, JWT_SECRET);
  //     return dataDecoded;
  //   } catch (error) {
  //     console.log('error', error);
  //     throw new HttpErrors.Unauthorized('');
  //   }
  // }
  decodeToken(token: string): string | null {
    try {
      const dataDecoded: DecodeTokenType = jsonWebToken.verify(
        token,
        JWT_SECRET,
      ) as DecodeTokenType;
      return dataDecoded.id;
    } catch (error) {
      return null;
    }
  }
}
