import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {HttpErrors} from '@loopback/rest';
import _ from 'lodash';
import {User} from '../models';
import {UserRepository} from '../repositories';
import {BcryptHasher} from './hash.password.bcryptjs';

export class UserManagementService {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject('services.hash.password')
    public passwordHasher: BcryptHasher,
  ) {}

  async createUser(user: Omit<User, 'id'>) {
    const hashPassword: string = await this.passwordHasher.hashPassword(
      user.password,
    );
    user.password = hashPassword;
    await this.userRepository.create(user);
    return _.omit(user, 'password');
  }

  async verifyCredentials(credentials: Pick<User, 'email' | 'password'>) {
    const {email, password} = credentials;

    const invalidCredentialsError = 'Invalid email or password.';

    const foundUser = await this.userRepository.findOne({
      where: {email},
    });
    if (!foundUser) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }

    const credentialFound = await this.userRepository.findById(foundUser.id);
    console.log('credentialFound', credentialFound.password);

    const passwordMatched = await this.passwordHasher.comparePassword(
      password,
      credentialFound.password,
    );

    if (!passwordMatched) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }

    return foundUser;
  }
}
