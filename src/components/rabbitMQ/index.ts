import {
  BindingScope,
  Component,
  inject,
  injectable,
  LifeCycleObserver,
} from '@loopback/core';
import {HOST_AMQP} from '../../config';
import {
  RabbitmqBindings,
  RabbitmqConsumer,
  RabbitmqProducer,
} from '../../packages/rabbitMQ';

const pubKey = {
  TENANT_UPDATED: 'TENANT_UPDATED',
};

type TenantUpdateRabbit = {
  name: string;
};

@injectable({scope: BindingScope.TRANSIENT})
export class RabbitConfigComponent implements Component, LifeCycleObserver {
  constructor(
    @inject(RabbitmqBindings.RABBITMQ_PRODUCER)
    private rabbitmqProducer: RabbitmqProducer,
    @inject(RabbitmqBindings.RABBITMQ_CONSUMER)
    private rabbitmqConsumer: RabbitmqConsumer,
  ) {}

  async start() {
    console.log('Rabbit component: start');

    this.rabbitmqConsumer.createAmqpSubcriber(HOST_AMQP, {
      [pubKey.TENANT_UPDATED]: data => {
        const dt = data as TenantUpdateRabbit;
        console.log('new message ' + dt.name);
      },
    });
  }

  async stop() {
    console.log('Rabbit component: stop');
  }
}
