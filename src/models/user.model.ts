import {Entity, model, property} from '@loopback/repository';

@model()
export class User extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  username: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'string',
  })
  fullName?: string;

  @property({
    type: 'string',
  })
  avatar?: string;

  @property({
    type: 'number',
  })
  phone?: number;

  @property({
    type: 'date',
  })
  birthday?: string;

  @property({
    type: 'string',
  })
  bio?: string;

  @property({
    type: 'object',
  })
  tenant?: object;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  lastLoginAt?: string;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  createAt?: Date;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: string;

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
