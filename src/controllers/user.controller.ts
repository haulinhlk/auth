import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  HttpErrors,
  post,
  Request,
  requestBody,
  RestBindings,
  SchemaObject,
} from '@loopback/rest';
import _ from 'lodash';
import {User} from '../models';
import {UserRepository} from '../repositories';
import {JWTService, UserManagementService} from '../services';

export const CredentialsSchema: SchemaObject = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      format: 'email',
    },
    password: {
      type: 'string',
      minLength: 8,
    },
  },
};

export class UserController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject('services.user')
    public userManagementService: UserManagementService,
    @inject('services.jwt')
    public jwtService: JWTService,
    @inject(RestBindings.Http.REQUEST) private request: Request,
  ) {}

  @post('/users', {
    responses: {
      '200': {
        description: 'Create new user',
        content: {'application/json': {schema: getModelSchemaRef(User)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, {
            title: 'NewUser',
            exclude: ['id'],
          }),
        },
      },
    })
    user: Omit<User, 'id'>,
  ): Promise<Omit<User, 'password'>> {
    return this.userManagementService.createUser(user);
  }

  @post('/users/login', {
    responses: {
      '200': {
        description: 'token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async login(
    @requestBody({
      content: {
        'application/json': {
          schema: CredentialsSchema,
        },
      },
    })
    credentials: Pick<User, 'email' | 'password'>,
  ): Promise<{token: string}> {
    const user = await this.userManagementService.verifyCredentials(
      credentials,
    );

    // convert a User object into a UserProfile object (reduced set of properties)
    const userProfile = _.pick(user, ['id', 'password']);
    console.log('userProfile', userProfile);

    // create a JSON Web Token based on the user profile
    const token = await this.jwtService.generateToken(userProfile);
    console.log('token', token);

    return {token};
  }

  @get('/me', {
    responses: {
      '200': {
        description: 'get me',
      },
    },
  })
  async getMe() {
    try {
      // console.log('header', this.request.headers.authorization);
      const token = this.request.headers.authorization ?? undefined;
      if (token === undefined)
        throw new HttpErrors.Unauthorized('ko cos token nhe ku');

      const userId = this.jwtService.decodeToken(token);
      if (userId === null) {
        throw new HttpErrors.Unauthorized('token het han cmnr');
      }

      const userInfo = await this.userRepository.findById(userId);

      const userInfoResponse = _.omit(userInfo, ['password']);

      return userInfoResponse;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // @patch('/users/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'User PATCH success',
  //     },
  //   },
  // })
  // async updateById(
  //   @param.path.string('id') id: string,
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(User, {partial: true}),
  //       },
  //     },
  //   })
  //   user: User,
  // ): Promise<void> {
  //   await this.userRepository.updateById(id, user);
  // }

  // @put('/users/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'User PUT success',
  //     },
  //   },
  // })
  // async replaceById(
  //   @param.path.string('id') id: string,
  //   @requestBody() user: User,
  // ): Promise<void> {
  //   await this.userRepository.replaceById(id, user);
  // }

  // @del('/users/{id}', {
  //   responses: {
  //     '204': {
  //       description: 'User DELETE success',
  //     },
  //   },
  // })
  // async deleteById(@param.path.string('id') id: string): Promise<void> {
  //   await this.userRepository.deleteById(id);
  // }
}
